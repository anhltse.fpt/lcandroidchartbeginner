# LCAndroidChartBeginner

Please clone this repo into your new repo and complete the below tasks.

- Focus on FirstFragment
- The code completed
    + neccessary libraries (gson, retrofit, MPChart) 
    + the request 1 week data with interval 30m (line: 58. FirstFragment)
    + the placeholder for LineChart
- Please convert the requested data into LineChart

Requirement for charts:
- Trục Ox hiện ngày của Chart
- Chart có full width, height = width
- Chart chỉ có 1 trục bên phải thôi
- Chart có 200 điểm.
- Chart là 1 đường Line bình thường
- Trục Oy bên phải có label nằm bên trong
- Bỏ Description của chart + chữ BTC Price (USD)