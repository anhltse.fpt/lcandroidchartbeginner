package com.example.lc_request_chart.services;

import com.example.lc_request_chart.models.ChartResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface BitscreenerAPI {
    @GET("/api/v4/graphs/ohlc/bitcoin")
    Call<ChartResponse> getChart(@Query("from") Long from, @Query("interval") String interval);
}
