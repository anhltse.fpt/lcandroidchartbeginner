package com.example.lc_request_chart.models;


import com.google.gson.annotations.SerializedName;

public class ChartResponse {
    @SerializedName("ts")
    Long[] ts;

    @SerializedName("open")
    Double[] open;

    @SerializedName("high")
    Double[] high;

    @SerializedName("low")
    Double[] low;

    @SerializedName("close")
    Double[] close;

    public Long[] getTs() {
        return ts;
    }

    public Double[] getOpen() {
        return open;
    }

    public Double[] getHigh() {
        return high;
    }

    public Double[] getLow() {
        return low;
    }

    public Double[] getClose() {
        return close;
    }
}
