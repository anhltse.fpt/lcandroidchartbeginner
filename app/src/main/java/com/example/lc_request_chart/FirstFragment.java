package com.example.lc_request_chart;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.lc_request_chart.databinding.FragmentFirstBinding;
import com.example.lc_request_chart.models.ChartResponse;
import com.example.lc_request_chart.services.BitscreenerAPI;
import com.github.mikephil.charting.charts.LineChart;
import java.util.Date;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class FirstFragment extends Fragment {
    private FragmentFirstBinding binding;
    LineChart lineChart;

    public static final String TAG = "😀😀😀😀";


    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://api-web.bitscreener.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentFirstBinding.inflate(inflater, container, false);
        lineChart = binding.lineChart;
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        BitscreenerAPI bitscreenerAPI = retrofit.create(BitscreenerAPI.class);

        Date currentDate = new Date();
        final Long ONE_DAY = 24L * 60L * 60L * 1000L;
        final Long PREVIOUS_7_DAYS = 7L * ONE_DAY;
        Long currentTimeLong = currentDate.getTime() - PREVIOUS_7_DAYS;

        bitscreenerAPI.getChart(currentTimeLong, "30m").enqueue(new Callback<ChartResponse>() {
            @Override
            public void onResponse(Call<ChartResponse> call, Response<ChartResponse> response) {
                Log.d(TAG, String.valueOf(response.body().getClose().length));

                // TODO
                // Add close data into Chart => Create LineChart
            }

            @Override
            public void onFailure(Call<ChartResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}